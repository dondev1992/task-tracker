import './TaskTracker.css';
import ChooseTask from './ChooseTask';

import React from 'react'

const TaskTracker = () => {
    return (
        <div className='taskTracker__container'>
            <h1>Task Tracker</h1>
            <ChooseTask />

        </div>
    )
}

export default TaskTracker