import { useState } from 'react';
import Task from './Task';
import './ChooseTask.css';

const ChooseTask = () => {

    const [taskList, taskListSet] = useState([])
    const [taskItem, taskItemSet] = useState("");

    const addTask = () => {
        taskListSet([...taskList, taskItem])
        taskItemSet("")
    }

    const removeTask = (id) => taskListSet(taskList.filter((t, index) => id !== index))

    return (
        <div className='chooseTask__container'>
            <h3>Create a task:</h3>
            <div className='chooseTask__input--container'>
                <input className='chooseTask__input' onChange={(e) => taskItemSet(e.target.value)} value={taskItem} name='select' list='select' />
                <p className='chooseTask__add--button' type='submit' onClick={(taskItem === "") ? null : addTask}>Add+</p>
            </div>
            <div className='chooseTask__taskList--container'>
                {taskList && (taskList.map((task, index) => (
                    <div className='chooseTask__taskList--task' key={index}>
                        <Task task={task} removeTask={removeTask} id={index} />
                    </div>
                )))}
            </div>
        </div>
    )
}

export default ChooseTask