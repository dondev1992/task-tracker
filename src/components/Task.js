import './Task.css';

const Task = ({ task, removeTask, id }) => {

    return (
        <div className='task__container'>
            <p className='task__name'>{task}</p>
            <p className='task__button' onClick={() => removeTask(id)}>Remove</p>
        </div>
    )
}

export default Task